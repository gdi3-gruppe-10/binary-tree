#pragma once

#include "SearchTree.hpp"

#include <random>
#include <cstdint>
#include <unordered_set>

/**
 * Testet, ob alle Werte in <keys> auch im Baum <tree> vorhanden sind
*/
template<typename KEY_TYPE>
bool test_all_present(SearchTree<KEY_TYPE>& tree, const std::unordered_set<KEY_TYPE>& keys) {
	for(const KEY_TYPE& key : keys)
		if(tree.search(key) == nullptr)
			return false;
	return true;
}


// generiert einen zufälligen Integer
template<typename T, std::enable_if_t<std::is_integral_v<T>, bool> = true>
T generateKey() {
	static std::default_random_engine generator;
	const std::uniform_int_distribution<T> distribution(std::numeric_limits<T>::min(), std::numeric_limits<T>::max());
	return distribution(generator);
}

// generiert einen zufälligen IEEE754 Float
template<typename T, std::enable_if_t<std::is_floating_point_v<T>, bool> = true>
T generateKey() {
	static std::default_random_engine generator;
	const std::uniform_real_distribution<T> distribution(std::numeric_limits<T>::min(), std::numeric_limits<T>::max());
	return distribution(generator);
}